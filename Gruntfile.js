/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
module.exports = function (grunt) {
    // Project configuration.
    grunt.initConfig({
		
		bower: {
			install: {
				target: {

					// Point to the files that should be updated when
					// you run `grunt bower-install`
					src: [
						'src/main/webapp/**/*.html'
					],

					// Optional:
					// ---------
					cwd: '',
					dependencies: true,
					devDependencies: false,
					exclude: [],
					fileTypes: {},
					ignorePath: '',
					overrides: {}
				}
			}
		}

    });
};