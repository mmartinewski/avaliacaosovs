package com.sovs.avaliacao.marcos.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;

/**
 *
 * @author Marcos Martinewski Alves
 */
public class JSONUtils {
    
    public static JSONObject toJSON(Object object) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return new JSONObject(mapper.writeValueAsString(object));
        } catch (JsonProcessingException ex) {
            throw new RuntimeException("Falha ao transformar o objeto em JSON. Objeto: " + object, ex);
        }
    }
    
}