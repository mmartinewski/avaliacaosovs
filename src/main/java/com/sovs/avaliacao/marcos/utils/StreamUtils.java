package com.sovs.avaliacao.marcos.utils;


import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 *
 * @author Marcos Martinewski Alves
 */
public class StreamUtils {
    
    public static void transferStream(InputStream inputStream, OutputStream outputStream) throws Exception {
        transferStream(inputStream, outputStream, 0);
    }
    
    public static String readString(InputStream inputStream) throws Exception {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        transferStream(inputStream, output);
        return new String(output.toByteArray());
    }

    public static void transferStream(InputStream inputStream, OutputStream outputStream, int totalSize) throws Exception {
        BufferedOutputStream output = null;
        BufferedInputStream input = null;
        try {
            double totalTransferido = 0;
            double doubleTotal = totalSize;
            byte[] buffer = new byte[2048];
            int lido;
            input = new BufferedInputStream(inputStream, buffer.length);
            output = new BufferedOutputStream(outputStream, buffer.length);
            while ((lido = input.read(buffer)) != -1) {
                output.write(buffer, 0, lido);
                totalTransferido += lido;
            }
            output.flush();
        } catch (Exception ex) {
            throw ex;
        } finally {
            try {
                if (input != null) {
                    input.close();
                }
            } catch (IOException ex) {
            }
            try {
                if (output != null) {
                    output.close();
                }
            } catch (IOException ex) {
                throw new RuntimeException("Failed to transfer stream", ex);
            }
        }
    }
    
}
