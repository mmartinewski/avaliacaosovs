package com.sovs.avaliacao.marcos.server.service.json;

import javax.servlet.http.HttpServletRequest;
import org.json.JSONObject;

/**
 *
 * @author Marcos Martinewski Alves
 */
public interface JSONService {
    
    public JSONObject execute(HttpServletRequest request, JSONObject arguments);
    
}
