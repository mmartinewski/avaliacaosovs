package com.sovs.avaliacao.marcos.server.domain;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Marcos
 */
public class CarrinhoCompras {

    private Usuario usuario;
    private List<ItemCarrinhoCompras> itens = new ArrayList<>();

    private double valorTotal;

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public void setItens(List<ItemCarrinhoCompras> itens) {
        this.itens.clear();
        this.itens.addAll(itens);
    }

    public List<ItemCarrinhoCompras> getItens() {
        return itens;
    }

    public double getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(double valorTotal) {
        this.valorTotal = valorTotal;
    }
    
    public ItemCarrinhoCompras getItemPorId(int id) {
        for (ItemCarrinhoCompras item : itens) {
            if (item.getId() == id) {
                return item;
            }
        }
        return null;
    }

}
