package com.sovs.avaliacao.marcos.server.domain;

/**
 *
 * @author Marcos Martinewski Alves
 */
public class Usuario {

    private String email;
    private String nome;
    private String sobrenome;
    private String hashCodeSenha;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSobrenome() {
        return sobrenome;
    }

    public void setSobrenome(String sobrenome) {
        this.sobrenome = sobrenome;
    }

    public String getHashCodeSenha() {
        return hashCodeSenha;
    }

    public void setHashCodeSenha(String hashCodeSenha) {
        this.hashCodeSenha = hashCodeSenha;
    }
    
}
