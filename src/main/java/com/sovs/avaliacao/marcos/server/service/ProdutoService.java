package com.sovs.avaliacao.marcos.server.service;

import com.sovs.avaliacao.marcos.server.domain.Produto;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Marcos
 */
public class ProdutoService {
    
    private static final List<Produto> produtos = new ArrayList(); 
    
    static {
        addProduto("PC Alienware 2000", 3999.90);
        addProduto("Mouse", 15.90);
        addProduto("Teclado", 17.90);
        addProduto("Gabinete", 159.90);
    }
        
    
    private static void addProduto(String descricao, double precoSugerido) {
        Produto produto = new Produto();
        produto.setId(produtos.size());
        produto.setCodigo(produtos.size()+"");
        produto.setDescricao(descricao);
        produto.setPrecoSugerido(precoSugerido);
        produtos.add(produto);
    }
    
    public Produto getProdutoById(int id) {
        for (Produto produto : produtos) {
            if (produto.getId() == id) {
                return produto;
            }
        }
        throw new RuntimeException("Produto com id " + id + " n�o encontrado");
    }
    
    public List<Produto> consultaProdutos(String filtro) {
        return produtos;
    }
    
}
