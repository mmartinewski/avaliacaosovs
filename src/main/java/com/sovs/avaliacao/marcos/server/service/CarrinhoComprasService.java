package com.sovs.avaliacao.marcos.server.service;

import com.sovs.avaliacao.marcos.server.domain.CarrinhoCompras;
import com.sovs.avaliacao.marcos.server.domain.ItemCarrinhoCompras;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.json.JSONObject;

/**
 *
 * @author Marcos
 */
public class CarrinhoComprasService {

    public CarrinhoCompras inicializaCarrinhoCompras(HttpServletRequest request) {
        HttpSession session = request.getSession();
        new LoginService().verificaUsuarioLogado(request);
        CarrinhoCompras carrinho = (CarrinhoCompras) session.getAttribute("carrinhoCompras");
        if (carrinho == null) {
            carrinho = new CarrinhoCompras();
            session.setAttribute("carrinhoCompras", carrinho);
        }
        return carrinho;
    }

    public CarrinhoCompras getCarrinhoCompras(HttpServletRequest request, boolean inicializar) {
        new LoginService().verificaUsuarioLogado(request);
        if (inicializar) {
            inicializaCarrinhoCompras(request);
        }
        return (CarrinhoCompras) request.getSession().getAttribute("carrinhoCompras");
    }

    public void adicionaItem(HttpServletRequest request, ItemCarrinhoCompras item) {
        CarrinhoCompras carrinho = getCarrinhoCompras(request, true);
        item.setId(carrinho.getItens().size());
        carrinho.getItens().add(item);
    }

    public void removeItem(HttpServletRequest request, int idItem) {
        CarrinhoCompras carrinho = getCarrinhoCompras(request, true);
        ItemCarrinhoCompras item = carrinho.getItemPorId(idItem);
        if (item != null) {
            carrinho.getItens().remove(item);
        }
    }

    public void limpaItens(HttpServletRequest request) {
        getCarrinhoCompras(request, true).getItens().clear();
    }

    private JSONObject geraVenda(HttpServletRequest request, CarrinhoCompras carrinhoCompras) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
