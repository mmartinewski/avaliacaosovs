package com.sovs.avaliacao.marcos.server.service;

import com.sovs.avaliacao.marcos.server.domain.Usuario;
import com.sovs.avaliacao.marcos.utils.JSONUtils;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.json.JSONObject;

/**
 *
 * @author Marcos
 */
public class LoginService {

    public Usuario login(HttpServletRequest request, String email, String senha) {
        HttpSession session = request.getSession();
        session.setAttribute("usuario", null);
        if (email == null || senha == null) {
            throw new RuntimeException("Por favor, informe usu�rio e senha");
        }
        String hashCodeSenha = senha.hashCode()+"";
        if (!hashCodeSenha.equals("12345".hashCode() + "")) {
            throw new RuntimeException("Usu�rio ou senha inv�lidos");
        }
        Usuario usuario = new Usuario();
        usuario.setEmail(email);
        usuario.setNome("Marcos");
        usuario.setSobrenome("Martinewski Alves");
        session.setAttribute("usuario", usuario);
        return usuario;
    }
    
    public void verificaUsuarioLogado(HttpServletRequest request) {
        HttpSession session = request.getSession();
        if (session == null) {
            throw new RuntimeException("Sess�o n�o inciada");
        }
        Usuario usuario = (Usuario) session.getAttribute("usuario");
        if (usuario == null) {
            throw new RuntimeException("Login n�o efetuado");
        }
    }
    
    public Usuario getUsuarioLogado(HttpServletRequest request) {
        HttpSession session = request.getSession();
        if (session != null) {
            return  (Usuario) session.getAttribute("usuario");
        }
        return null;
    }
    
    public void encerraSessao(HttpServletRequest request) {
        if (request.getSession() != null) {
            request.getSession().invalidate();
        }
    }

}
