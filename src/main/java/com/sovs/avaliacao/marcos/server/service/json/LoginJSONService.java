package com.sovs.avaliacao.marcos.server.service.json;

import com.sovs.avaliacao.marcos.server.domain.Usuario;
import com.sovs.avaliacao.marcos.server.service.CarrinhoComprasService;
import com.sovs.avaliacao.marcos.server.service.LoginService;
import com.sovs.avaliacao.marcos.utils.JSONUtils;
import javax.servlet.http.HttpServletRequest;
import org.json.JSONObject;

/**
 *
 * @author Marcos Martinewski Alves
 */
public class LoginJSONService implements JSONService {

    @Override
    public JSONObject execute(HttpServletRequest request, JSONObject arguments) {
        JSONObject data = arguments.getJSONObject("data");
        if (!data.has("operacao")) {
            throw new RuntimeException("Opera��o n�o informada");
        }

        String operacao = data.getString("operacao");
        switch (operacao) {
            case "login":
                return login(request, arguments);
            case "getUsuarioLogado":
                return getUsuarioLogado(request, arguments);
            case "encerraSessao":
                return encerraSessao(request, arguments);
            default:
                throw new RuntimeException("Opera��o \"" + operacao + "\" n�o esperada");
        }
    }

    private JSONObject login(HttpServletRequest request, JSONObject arguments) {
        JSONObject data = arguments.getJSONObject("data");
        String email = data.has("email") ? data.getString("email") : null;
        String senha = data.has("senha") ? data.getString("senha") : null;
        
        LoginService service = new LoginService();
        Usuario usuario = service.login(request, email, senha);
        
        JSONObject result = new JSONObject();
        result.put("valido", true);
        result.put("usuario", JSONUtils.toJSON(usuario));
        result.put("carrinhoCompras", JSONUtils.toJSON(new CarrinhoComprasService().getCarrinhoCompras(request, true)));
        
        return result;
    }

    private JSONObject getUsuarioLogado(HttpServletRequest request, JSONObject arguments) {
        LoginService service = new LoginService();
        Usuario usuario = service.getUsuarioLogado(request);
        
        JSONObject rtn = new JSONObject();
        if (usuario != null) {
            rtn.put("usuario", JSONUtils.toJSON(usuario));
            rtn.put("carrinhoCompras", JSONUtils.toJSON(new CarrinhoComprasService().getCarrinhoCompras(request, true)));
        }
        rtn.put("sessionId", request.getSession().getId());
        
        return rtn;
    }

    private JSONObject encerraSessao(HttpServletRequest request, JSONObject arguments) {
        LoginService service = new LoginService();
        service.encerraSessao(request);
        return null;
    }

}
