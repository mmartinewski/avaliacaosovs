package com.sovs.avaliacao.marcos.server.servlets;

import com.sovs.avaliacao.marcos.server.service.json.JSONService;
import com.sovs.avaliacao.marcos.utils.StreamUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author Marcos
 */
@WebServlet(name="Control", urlPatterns = {"/Control"})
public class Control extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("processRequest");
        if ("JSONService".equals(request.getParameter("type"))) {
            resolveJSONServiceRequest(request, response);
        }
    }

    private void resolveJSONServiceRequest(HttpServletRequest request, HttpServletResponse response) {
        try {
            response.setContentType("application/json;charset=UTF-8");
            
            JSONObject jsonRequestData = new JSONObject(StreamUtils.readString(request.getInputStream()));
            
            String classeServico = jsonRequestData.getString("service");
            if (classeServico == null) {
                throw new Exception("Nome do servi�o n�o informado");
            }
            
            System.out.println("classeServico: " + classeServico);
            JSONService service = getJSONServiceFor(classeServico);
            JSONObject result;
            try {
                result = service.execute(request, jsonRequestData);
            } catch (Exception ex) {
                JSONObject jsonError = new JSONObject();
                jsonError.put("message", ex.getMessage());
                jsonError.put("stackTrace", getStackTrace(ex));
                
                result = new JSONObject();
                result.put("error", jsonError);
            }
            try (PrintWriter out = response.getWriter()) {
                out.print(result);
            }
        } catch (Exception ex) {
            throw new RuntimeException("Falha ao processar a requisi��o", ex);
        }
    }
    
    private String getStackTrace(Throwable th) {
        StringWriter sw = new StringWriter();
        th.printStackTrace(new PrintWriter(sw));
        return sw.toString();
    }

    private JSONService getJSONServiceFor(String serviceSimpleClassName) {
        try {
            Class serviceClass = Class.forName("com.sovs.avaliacao.marcos.server.service.json." + serviceSimpleClassName);
            return (JSONService) serviceClass.newInstance();
        } catch (ClassNotFoundException ex) {
            throw new RuntimeException("Servi�o \"" + serviceSimpleClassName + "\" n�o declarado");
        } catch (InstantiationException ex) {
            throw new RuntimeException("Falha ao instanciar o servi�o \"" + serviceSimpleClassName + "\"");
        } catch (IllegalAccessException ex) {
            throw new RuntimeException("Acesso negado ao servi�o \"" + serviceSimpleClassName + "\"");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
