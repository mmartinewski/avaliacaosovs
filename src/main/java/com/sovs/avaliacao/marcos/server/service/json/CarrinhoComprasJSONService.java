package com.sovs.avaliacao.marcos.server.service.json;

import com.sovs.avaliacao.marcos.server.domain.CarrinhoCompras;
import com.sovs.avaliacao.marcos.server.domain.ItemCarrinhoCompras;
import com.sovs.avaliacao.marcos.server.domain.Produto;
import com.sovs.avaliacao.marcos.server.service.CarrinhoComprasService;
import com.sovs.avaliacao.marcos.server.service.ProdutoService;
import com.sovs.avaliacao.marcos.utils.JSONUtils;
import javax.servlet.http.HttpServletRequest;
import org.json.JSONObject;

/**
 *
 * @author Marcos
 */
public class CarrinhoComprasJSONService implements JSONService {

    @Override
    public JSONObject execute(HttpServletRequest request, JSONObject arguments) {
        JSONObject data = arguments.getJSONObject("data");
        if (!data.has("operacao")) {
            throw new RuntimeException("Opera��o n�o informada");
        }

        String operacao = data.getString("operacao");
        switch (operacao) {
            case "adicionaItem":
                return adicionaItem(request, arguments);
            case "removeItem":
                return removeItem(request, arguments);
            case "getCarrinhoCompras":
                return getCarrinhoCompras(request, arguments);
            case "limpaItems":
                return limpaItens(request, arguments);
            case "geraVenda":
                return geraVenda(request, arguments);
            default:
                throw new RuntimeException("Opera��o \"" + operacao + "\" n�o esperada");
        }
    }

    private JSONObject adicionaItem(HttpServletRequest request, JSONObject arguments) {
        
        JSONObject data = arguments.getJSONObject("data");
        
        if (!data.has("item")) {
            throw new RuntimeException("Item n�o informado");
        }
        
        JSONObject jsonItem = data.getJSONObject("item");
        if (!jsonItem.has("idProduto")) {
            throw new RuntimeException("Produto do item n�o informado");
        }
        if (!jsonItem.has("quantidade")) {
            throw new RuntimeException("Quantidade do item n�o informada");
        }
        
        Produto produto = new ProdutoService().getProdutoById(jsonItem.getInt("idProduto"));
        
        ItemCarrinhoCompras item = new ItemCarrinhoCompras();
        item.setProduto(produto);
        item.setQuantidade(jsonItem.getInt("quantidade"));
        item.setValorUnitario(produto.getPrecoSugerido());
        item.setValorTotal(item.getQuantidade() * item.getValorUnitario());
        
        new CarrinhoComprasService().adicionaItem(request, item);
        
        JSONObject rtn = new JSONObject();
        rtn.put("item", JSONUtils.toJSON(item));
        return rtn;
    }

    private JSONObject removeItem(HttpServletRequest request, JSONObject arguments) {
        JSONObject data = arguments.getJSONObject("data");
        
        if (!data.has("idItem")) {
            throw new RuntimeException("Id do item n�o informado");
        }
        
        CarrinhoComprasService service = new CarrinhoComprasService();
        service.removeItem(request, data.getInt("idItem"));
        return JSONUtils.toJSON(service.getCarrinhoCompras(request, true));
    }

    private JSONObject getCarrinhoCompras(HttpServletRequest request, JSONObject arguments) {
        return JSONUtils.toJSON(new CarrinhoComprasService().getCarrinhoCompras(request, true));
    }

    private JSONObject limpaItens(HttpServletRequest request, JSONObject arguments) {
        new CarrinhoComprasService().limpaItens(request);
        return null;
    }

    private JSONObject geraVenda(HttpServletRequest request, JSONObject arguments) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    

}
