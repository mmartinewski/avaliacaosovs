angular.module('app', ['ngMaterial']).controller(
        "appController", function ($scope, $mdDialog, $http) {
            var mainScope = $scope;
            
            $scope.usuario = null;
            $scope.carrinhoCompras = null;
            
            $scope.showLoginDialog = function (evt) {
                $mdDialog.show({
                    controller: LoginDialogController,
                    templateUrl: 'login.tmpl.html',
                    parent: angular.element(document.body),
                    targetEvent: evt,
                    clickOutsideToClose: true,
                    fullscreen: $scope.customFullscreen
                })
                .then(function (answer) {
                    $scope.status = 'You said the information was "' + answer + '".';
                }, function () {
                    $scope.status = 'You cancelled the dialog.';
                });
                
                function LoginDialogController($scope, $mdDialog) {
                        
                        $scope.loginData = {
                            email: "",
                            password: "",
                            status: " "
                        };
                        
                        $scope.confirmar = function () {
                            $scope.executaLogin();
                        };
                        
                        $scope.executaLogin = function () {
                            console.log($scope.loginData);
                            $scope.loginData.status = "Verificando usu�rio e senha";
                            $http.post("Control?type=JSONService", {service: "LoginJSONService", data: {
                                operacao: "login",
                                email: $scope.loginData.email,
                                senha: $scope.loginData.password
                            }})
                            .then(function (response) {
                                if (response.data.valido) {
                                    console.log("Deu certo");
                                    console.log(response);
                                    mainScope.atualizaDadosUsuario(response.data);
                                    $mdDialog.hide();
                                } else {
                                    console.log("Erro");
                                    console.log(response);
                                    mainScope.usuario = null;
                                    mainScope.carrinhoCompras = null;
                                    $scope.loginData.status = "Verifique usuario e senha";
                                }
                            }, function (response) {
                                console.log("Erro");
                                console.log(response);
                                mainScope.usuario = null;
                                $scope.loginData.status = "Ocorreu um erro ao efetuar o login";
                            });
                        };
                        $scope.cancelar = function () {
                            $mdDialog.hide();
                        };
                    }
                
            };
            
            $scope.adicionaItemCarrinho = function () {
                $http.post("Control?type=JSONService", {service: "CarrinhoComprasJSONService", data: {
                    operacao: "adicionaItem",
                    item: {
                        idProduto: "0",
                        quantidade: 1
                    }
                }})
                .then(function (response) {
                    console.log("Resposta adi��o item");
                    console.log(response);
                    mainScope.carrinhoCompras.itens.push(response.data.item);
                }, function (response) {
                    console.log("Erro adi��o item");
                    console.log(response);
                });
            };
            
            $scope.removeItemCarrinho = function (item) {
                 $http.post("Control?type=JSONService", {service: "CarrinhoComprasJSONService", data: {
                    operacao: "removeItem",
                    idItem: item.id
                }})
                .then(function (response) {
                    $scope.verificaUsuarioLogado();
                }, function (response) {
                    console.log(response);
                });
            };
            
            $scope.verificaUsuarioLogado = function () {
                $http.post("Control?type=JSONService", {service: "LoginJSONService", data: {
                    operacao: "getUsuarioLogado"
                }})
                .then(function (response) {
                    $scope.atualizaDadosUsuario(response.data);
                }, function (response) {
                });
            };
            $scope.verificaUsuarioLogado();
            
            $scope.encerraSessao = function () {
                $http.post("Control?type=JSONService", {service: "LoginJSONService", data: {
                    operacao: "encerraSessao"
                }})
                .then(function (response) {
                    mainScope.usuario = null;
                    mainScope.carrinhoCompras = null;
                }, function (response) {
                    mainScope.usuario = null;
                    mainScope.carrinhoCompras = null;
                });
            };
            
            $scope.atualizaDadosUsuario = function (data) {
                if (data.usuario != null) {
                    mainScope.usuario = data.usuario;
                }
                if (data.carrinhoCompras != null) {
                    mainScope.carrinhoCompras = data.carrinhoCompras;
                }
            };
            
        }
);
